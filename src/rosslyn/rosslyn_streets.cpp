#include <geometry.h>
#include <output.h>

#include <rxapi/Node.h>
#include <rxapi/Primitive.h>
#include <rxapi/ProgressReporter.h>
#include <rxapi/Vector.h>

#include <rxapischema/CartesianPoint.h>
#include <rxapischema/City.h>
#include <rxapischema/DoubleOutputColumn.h>
#include <rxapischema/Job.h>
#include <rxapischema/OutputRequest.h>
#include <rxapischema/PointSet.h>
#include <rxapischema/PropagationPathRequest.h>
#include <rxapischema/Receiver.h>
#include <rxapischema/RouteSet.h>
#include <rxapischema/Scene.h>
#include <rxapischema/ShortDipole.h>
#include <rxapischema/Sinusoid.h>
#include <rxapischema/Terrain.h>
#include <rxapischema/TxRxSetList.h>
#include <rxapischema/X3D.h>

#ifdef _WIN32
    #define DATA_DIR "..\\..\\..\\..\\data\\"
#else
    // for Linux distributions, please configure DATA_DIR as necessary
    #define DATA_DIR "../../data/"
#endif
#define RESULTS_DIR "../../results/"

static const double originLon = -77.0787;
static const double originLat = 38.8907;

int
main( int argc, char** argv )
{
    // a progress reporter must be instantiated and registered with the rxapi to provide error, warning, and progress messages
    remcom::rxapi::ProgressReporter progressReporter( NULL );
    remcom::rxapi::Factory::instance( ).setProgressReporter( progressReporter );

    // create the scene
    remcom::rxapi::SceneHandle scene = remcom::rxapi::Scene::New( );

    // set the scene's origin to match the Wireless InSite project's origin
    scene->getOrigin( )->setLongitude( originLon );
    scene->getOrigin( )->setLatitude( originLat );

    // create geometry
    
    // add the rosslyn city - this is the city node itself, and we will assign its source next
    remcom::rxapi::CityHandle city = remcom::rxapi::City::New( ); 
    
    // we are loading a native Wireless InSite .city file, so we create a WirelessInSiteGeometryHandle
    remcom::rxapi::WirelessInSiteGeometryHandle citySource = remcom::rxapi::WirelessInSiteGeometry::New( );
    remcom::rxapi::RString cityFilename( remcom::rxapi::RString( std::string( DATA_DIR ) + "rosslyn_v3.city" ) );
    citySource->getFilename( )->getFilename( )->setValue( cityFilename );

    // assign the source
    city->setGeometrySource( citySource );

    // add the realistic rosslyn terrain
    remcom::rxapi::TerrainHandle terrain = remcom::rxapi::Terrain::New( );

    // again, since we are using a native Wireless InSite geometry file, create a WirelessInSiteGeometryHandle
    remcom::rxapi::WirelessInSiteGeometryHandle terrainSource = remcom::rxapi::WirelessInSiteGeometry::New( );
    remcom::rxapi::RString terrainFilename( remcom::rxapi::RString( std::string( DATA_DIR ) + "Rosslyn_Realistic_Terrain.ter" ) );
    terrainSource->getFilename( )->getFilename( )->setValue( terrainFilename );

    // assign the source
    terrain->setGeometrySource( terrainSource );
    
    // add geometry
    scene->getGeometryList( )->addGeometry( city );
    scene->getGeometryList( )->addGeometry( terrain );

    // add transmitters/receivers
    // add Tx 2B from the Wireless InSite project
    remcom::rxapi::PointSetHandle tx2b = remcom::rxapi::PointSet::New( );
    tx2b->setOutputID( 1 );
    
    // set Tx 2B's control point
    remcom::rxapi::CartesianPointHandle point = remcom::rxapi::CartesianPoint::New( );
    point->setX( 736.4469 );
    point->setY( 667.8848 );
    point->setZ( 10.0 );

    tx2b->getControlPoints( )->setProjectedPoint( point );
    tx2b->setConformToTerrain( true );

    // add the Lynn St receiver route from the Wireless InSite project
    remcom::rxapi::RouteSetHandle lynnSt = remcom::rxapi::RouteSet::New( );
    lynnSt->setOutputID( 6 );
    lynnSt->setConformToTerrain( true );

    remcom::rxapi::CartesianPointHandle startPointLynnSt = remcom::rxapi::CartesianPoint::New( );
    remcom::rxapi::CartesianPointHandle stopPointLynnSt = remcom::rxapi::CartesianPoint::New( );

    startPointLynnSt->setX( 646.5510 );
    startPointLynnSt->setY( 482.6249 );
    startPointLynnSt->setZ( 2.0 );

    stopPointLynnSt->setX( 643.4868 );
    stopPointLynnSt->setY( 773.2593 );
    stopPointLynnSt->setZ( 2.0 );
    
    // we can call setProjectedPoint( ) to set the first point in the list
    lynnSt->getControlPoints( )->setProjectedPoint( startPointLynnSt );

    // we need to call addProjectedPoint( ) to add subsequent points
    lynnSt->getControlPoints( )->addProjectedPoint( stopPointLynnSt );

    // set the spacing
    lynnSt->setSpacing( 20.0 );

    // add the N Kent St receiver route from the Wireless InSite project
    remcom::rxapi::RouteSetHandle nKentSt = remcom::rxapi::RouteSet::New( );
    nKentSt->setOutputID( 8 );
    nKentSt->setConformToTerrain( true );

    remcom::rxapi::CartesianPointHandle startPointKentSt = remcom::rxapi::CartesianPoint::New( );
    remcom::rxapi::CartesianPointHandle stopPointKentSt = remcom::rxapi::CartesianPoint::New( );

    startPointKentSt->setX( 766.5859 );
    startPointKentSt->setY( 451.3697 );
    startPointKentSt->setZ( 2.0 );

    stopPointKentSt->setX( 748.5018 );
    stopPointKentSt->setY( 655.7868 );
    stopPointKentSt->setZ( 2.0 );
    
    // we can call setProjectedPoint( ) to set the first point in the list
    nKentSt->getControlPoints( )->setProjectedPoint( startPointKentSt );

    // we need to call addProjectedPoint( ) to add subsequent points
    nKentSt->getControlPoints( )->addProjectedPoint( stopPointKentSt );

    // set the spacing
    nKentSt->setSpacing( 20.0 );
    
    // create the vertical dipole antenna
    remcom::rxapi::ShortDipoleHandle verticalDipole = remcom::rxapi::ShortDipole::New( );
    
    // create and assign the 908-5 MHz waveform
    remcom::rxapi::SinusoidHandle sinusoid908 = remcom::rxapi::Sinusoid::New( );
    //sinusoid908->setCarrierFrequency( 908.0e6 );  // frequency is specified in Hz
    sinusoid908->setCarrierFrequency( 60.0e9 );  // frequency is specified in Hz
    sinusoid908->setBandwidth( 500.0 );
    
    verticalDipole->setWaveform( sinusoid908 );

    // create the transmitter properties
    remcom::rxapi::TransmitterHandle txProperties = remcom::rxapi::Transmitter::New( );
    txProperties->setAntenna( verticalDipole );
    txProperties->setInputPower( 1.0 );

    // zero out the antenna rotations
    txProperties->getAntennaRotations( )->setX( remcom::rxapi::Vector< double >( 1, 0.0 ) );
    txProperties->getAntennaRotations( )->setY( remcom::rxapi::Vector< double >( 1, 0.0 ) );
    txProperties->getAntennaRotations( )->setZ( remcom::rxapi::Vector< double >( 1, 0.0 ) );
    
    tx2b->setTransmitter( txProperties );
    
    // create the receiver properties
    remcom::rxapi::ReceiverHandle rxProperties = remcom::rxapi::Receiver::New( );
    rxProperties->setAntenna( verticalDipole );

    // zero out the antenna rotations
    rxProperties->getAntennaRotations( )->setX( remcom::rxapi::Vector< double >( 1, 0.0 ) );
    rxProperties->getAntennaRotations( )->setY( remcom::rxapi::Vector< double >( 1, 0.0 ) );
    rxProperties->getAntennaRotations( )->setZ( remcom::rxapi::Vector< double >( 1, 0.0 ) );
    
    lynnSt->setReceiver( rxProperties );
    nKentSt->setReceiver( rxProperties );

    // now add tx2b and lynnSt to the scene's list of transmitters and receivers
    scene->getTxRxSetList( )->addTxRxSet( tx2b );
    scene->getTxRxSetList( )->addTxRxSet( lynnSt );
    scene->getTxRxSetList( )->addTxRxSet( nKentSt );

    // create an x3d study area
    remcom::rxapi::X3DHandle x3d = remcom::rxapi::X3D::New( );
    //x3d->setUseGPU(false);
    

    // set the model parameters to something reasonably balanced between fidelity and speed
    x3d->setReflections( 8 );
    x3d->setTransmissions( 0 );
    x3d->setDiffractions( 0 );
    x3d->setCPUThreads( 16 );

    // now that we have a full scene and a model to run, create a job and hook everything together
    remcom::rxapi::JobHandle job = remcom::rxapi::Job::New( );
    job->setScene( scene );
    job->setModel( x3d );

    // the x3d model uses an SQLite database to store its paths
    remcom::rxapi::PathResultsDatabaseHandle database = remcom::rxapi::PathResultsDatabase::New( );
    job->setPathResultsDatabase( database );
    remcom::rxapi::FileDescriptionHandle databaseFilename = remcom::rxapi::FileDescription::New( );
    databaseFilename->setFilename( std::string(RESULTS_DIR) + "rosslyn_streets.sql" );
    database->setFilename( databaseFilename );

    // all requirements for running an x3d calculation should now be met, so check that the job is valid and then run
    if( !job->isValid( ) )
    {
        progressReporter.reportError( job->getReasonWhyInvalid( ) );
        return 1;
    }

    // at this point, the job is ready to run; it may be beneficial to serialize it to XML for viewing
    remcom::rxapi::Factory::instance( ).save( job, std::string(RESULTS_DIR) + "rosslyn_streets.xml", false );

    job->execute( &progressReporter );

    // once the job has finished, it will be updated with output, which can also be serialized
    remcom::rxapi::Factory::instance( ).save( job, std::string(RESULTS_DIR) + "rosslyn_streets_out.xml", false );

    if( !(job->getOutput( )->isValid( )) )
    {
        progressReporter.reportError( job->getOutput( )->getReasonWhyInvalid( ) );
        return 1;
    }

    // write output to the console
    writeAllChannelsToConsole( database, tx2b->getOutputID( )->getValue( ), lynnSt->getOutputID( )->getValue( ) );
    writeAllChannelsToConsole( database, tx2b->getOutputID( )->getValue( ), nKentSt->getOutputID( )->getValue( ) );

    return 0;
}
